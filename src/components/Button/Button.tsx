import './Button.scss';

interface ButtonProps {
    className?: string
    value: string
    color: 'telegram' | 'red'
    onClick: () => void
}

const Button = (props: ButtonProps) => {
    return (
        <button className={`button ${props.className ?? ''} ${props.color}`} onClick={props.onClick}>
            {props.value}
        </button>
    )
}

export default Button;
