import './CommentList.scss';
import {useState} from 'react';
import {shallowEqual, useDispatch} from 'react-redux';
import {useAppSelector} from '../../redux/useAppSelector';
import {Task} from '../../redux/reducers/tasksReducer';
import {Comment, DEFAULT_COMMENT} from '../../redux/reducers/commentsReducer';
import Input from '../Input/Input';
import Button from '../Button/Button';

interface CommentListProps {
    task: Task
    parentComment?: Comment
    level: number
}

const CommentList = (props: CommentListProps) => {
    const dispatch = useDispatch()
    const comments = useAppSelector(state => state.comments.list.filter(c =>
        c.taskId === props.task.id && c.parentCommentId === props.parentComment?.id
    ), shallowEqual)

    const [newComment, setNewComment] = useState(DEFAULT_COMMENT)
    const updateNewCommentText = (text: string) => setNewComment({...newComment, text: text})

    const addComment = () => {
        dispatch({
            type: 'ADD_COMMENT',
            payload: {
                ...newComment,
                id: Date.now(),
                taskId: props.task.id,
                parentCommentId: props.parentComment?.id,
            }
        });
        setNewComment(DEFAULT_COMMENT);
    }

    return (
        <div className={`comments-list ${props.parentComment ? 'subcomments' : ''} l-${props.level % 4 + 1}`}>
            {comments.map(comment =>
                <div key={comment.id} className='comments-list-element'>
                    {comment.text}
                    <CommentList task={props.task} parentComment={comment} level={props.level + 1}/>
                </div>
            )}
            <div className='comments-add'>
                <Input
                    className='comments-add-input'
                    value={newComment.text}
                    setValue={updateNewCommentText}
                    onEnter={addComment}
                    placeholder={props.parentComment?.text ? `Reply to '${props.parentComment.text}'` : ''}
                />
                <Button
                    className='comments-add-button'
                    value='Comment'
                    color='telegram'
                    onClick={addComment}
                />
            </div>
        </div>
    )
}

export default CommentList;
