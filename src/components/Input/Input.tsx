import './Input.scss';
import {KeyboardEvent, ChangeEvent, forwardRef, LegacyRef} from 'react';

interface InputProps {
    className?: string
    value: string
    setValue?: (value: string) => void
    placeholder?: string
    disabled?: boolean
    onEnter?: () => void
}

const Input = forwardRef((props: InputProps, ref: LegacyRef<HTMLInputElement> | undefined) => {
    const onChange = (e: ChangeEvent<HTMLInputElement>) => props.setValue && props.setValue(e.target.value)
    const onEnter = (e: KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') props.onEnter && props.onEnter();
    }
    return (
        <input
            className={`input ${props.className ?? ''}`}
            value={props.value}
            onChange={onChange}
            placeholder={props.placeholder}
            ref={ref}
            onKeyDown={onEnter}
            disabled={props.disabled}
        />
    )
})

export default Input;
