import './Modal.scss';
import {ReactNode, KeyboardEvent, MouseEvent} from 'react';
import Button from '../Button/Button';

export interface AnyModalProps {
    visible: boolean
    hide: () => void
    title?: string
    description?: string
    actionName?: string
    actionNegative?: boolean
}

interface ModalProps extends AnyModalProps {
    children?: string | ReactNode | ReactNode[]
    onAction: () => void
}

const Modal = (props: ModalProps) => {
    const onKeyDown = (e: KeyboardEvent<HTMLDivElement>) => {
        if (e.key === 'Escape') props.hide();
        else if (e.key === 'Enter') props.onAction();
    }

    const stopPropagation = (e: MouseEvent<HTMLDivElement>) => e.stopPropagation()

    return (
        <div className={`modal ${props.visible ? '' : 'hidden'}`} onClick={props.hide} onKeyDown={onKeyDown}>
            <div className='modal-window' onClick={stopPropagation}>
                <h2 className='title'>
                    {props.title}
                </h2>
                <div className='content'>
                    {props.description}
                    {props.children}
                </div>
                <div className='actions'>
                    <Button
                        value='Cancel'
                        color={props.actionNegative ? 'telegram' : 'red'}
                        onClick={props.hide}
                    />
                    <Button
                        value={props.actionName ?? ''}
                        color={props.actionNegative ? 'red' : 'telegram'}
                        onClick={props.onAction}
                    />
                </div>
            </div>
        </div>
    )
}

export default Modal;
