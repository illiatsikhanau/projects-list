import Modal, {AnyModalProps} from './Modal';

interface ModalConfirmProps extends AnyModalProps {
    onAction: () => void
}

const ModalConfirm = (props: ModalConfirmProps) => {
    const confirm = () => {
        props.onAction();
        props.hide();
    }

    return (
        <Modal
            visible={props.visible}
            hide={props.hide}
            title={props.title}
            description={props.description}
            actionName={props.actionName}
            onAction={confirm}
            actionNegative={props.actionNegative}
        />
    )
}

export default ModalConfirm;
