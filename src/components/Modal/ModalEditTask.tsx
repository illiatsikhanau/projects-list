import {useEffect, useState} from 'react';
import {PRIORITY_OPTIONS, Task} from '../../redux/reducers/tasksReducer';
import Modal, {AnyModalProps} from './Modal';
import Input from '../Input/Input';
import Select from '../Select/Select';

interface ModalEditTaskProps extends AnyModalProps {
    onAction: (values: Map<string, string>) => void
    task?: Task
    parentTask?: Task
}

const ModalEditTask = (props: ModalEditTaskProps) => {
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [priority, setPriority] = useState('')

    const saveTask = () => {
        const values = new Map();
        values.set('name', name);
        values.set('description', description);
        values.set('priority', priority);
        props.onAction(values);
        props.hide();
    }

    useEffect(() => {
        if (props.task) {
            setName(props.task.name);
            setDescription(props.task.description);
            setPriority(props.task.priority.toString());
        }
    }, [props.task])

    return (
        <Modal visible={props.visible} hide={props.hide} title='Edit task' actionName='Save' onAction={saveTask}>
            <div className='modal-element'>
                <div className='modal-label'>Name:</div>
                <Input value={name} setValue={setName} placeholder='Name'/>
            </div>
            <div className='modal-element'>
                <div className='modal-label'>Description:</div>
                <Input value={description} setValue={setDescription} placeholder='Description'/>
            </div>
            <div className='modal-element'>
                <div className='modal-label'>Priority:</div>
                <Select options={PRIORITY_OPTIONS} value={priority} setValue={setPriority}/>
            </div>
        </Modal>
    )
}

export default ModalEditTask;
