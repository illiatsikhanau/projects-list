import './ProjectEdit.scss';
import {useEffect, useRef, useState} from 'react';
import {useDispatch} from 'react-redux';
import {Project} from '../../redux/reducers/projectsReducer';
import Input from '../Input/Input';
import Button from '../Button/Button';

interface ProjectEditProps {
    project?: Project
    isVisible: boolean
    hide: () => void
}

const ProjectEdit = (props: ProjectEditProps) => {
    const dispatch = useDispatch()
    const inputRef = useRef<HTMLInputElement>(null)

    const [newProject, setNewProject] = useState(props.project)
    const updateNewProjectName = (name: string) => {
        if (newProject) setNewProject({...newProject, name: name});
    }

    const onReset = () => setNewProject(props.project)
    const onCancel = () => {
        props.hide();
        onReset();
    }
    const onSave = () => {
        dispatch({type: 'UPDATE_PROJECT', payload: newProject});
        props.hide();
    }

    useEffect(() => {
        if (props.isVisible && inputRef.current) inputRef.current.focus();
    }, [props.isVisible])

    return (
        <div className={`project-edit ${props.isVisible ? '' : 'hidden'}`}>
            <h3 className='project-edit-title'>Edit Project</h3>
            <div className='w-100'>
                <p className='label'>Name:</p>
                <Input
                    value={newProject?.name ?? ''}
                    setValue={updateNewProjectName}
                    onEnter={onSave}
                    ref={inputRef}
                />
            </div>
            <div className='project-edit-actions'>
                <Button
                    className='project-edit-action'
                    value='Cancel'
                    color='red'
                    onClick={onCancel}
                />
                <Button
                    className='project-edit-action'
                    value='Reset'
                    color='red'
                    onClick={onReset}
                />
                <Button
                    className='project-edit-action'
                    value='Save'
                    color='telegram'
                    onClick={onSave}
                />
            </div>
        </div>
    )
}

export default ProjectEdit;
