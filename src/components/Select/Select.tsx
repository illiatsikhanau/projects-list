import './Select.scss';
import {ChangeEvent, KeyboardEvent} from 'react';

export interface Option {
    value: string
    label: string
}

interface SelectProps {
    className?: string
    options: Option[]
    value: string
    setValue: (value: string) => void
    nullable?: boolean
}

const Select = (props: SelectProps) => {
    const onChange = (e: ChangeEvent<HTMLSelectElement>) => props.setValue(e.target.value)
    const onKeyDown = (e: KeyboardEvent<HTMLSelectElement>) => e.stopPropagation()

    return (
        <select
            className={`select ${props.className ?? ''}`}
            value={props.value}
            onChange={onChange}
            onKeyDown={onKeyDown}
        >
            {props.nullable ? <option className='option' value=''/> : ''}
            {props.options.map(option =>
                <option key={option.value} className='option' value={option.value}>
                    {option.label}
                </option>
            )}
        </select>
    )
}

export default Select;
