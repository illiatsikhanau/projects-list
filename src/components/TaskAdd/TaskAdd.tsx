import './TaskAdd.scss';
import {useEffect, useRef, useState} from 'react';
import {useDispatch} from 'react-redux';
import {DEFAULT_TASK, PRIORITY_OPTIONS, Task} from '../../redux/reducers/tasksReducer';
import Input from '../Input/Input';
import Select from '../Select/Select';
import Button from '../Button/Button';

interface TaskAddProps {
    projectId: number
    tasks: Task[]
    isVisible: boolean
    hide: () => void
}

const TaskAdd = (props: TaskAddProps) => {
    const dispatch = useDispatch()
    const inputRef = useRef<HTMLInputElement>(null)

    const [newTask, setNewTask] = useState(DEFAULT_TASK)
    const updateNewTaskName = (name: string) => setNewTask({...newTask, name: name})
    const updateNewTaskDescription = (description: string) => setNewTask({...newTask, description: description})
    const updateNewTaskPriority = (priority: string) => setNewTask({...newTask, priority: parseInt(priority)})
    const updateNewTaskParent = (parentId: string) => setNewTask({...newTask, parentTaskId: parseInt(parentId)})

    const onClear = () => setNewTask(DEFAULT_TASK)
    const onClose = () => {
        props.hide();
        onClear();
    }
    const onAdd = () => {
        const nextNumber = props.tasks.map(task => task.number)
            .reduce((prev, current) => current > prev ? current : prev, 0) + 1;

        dispatch({
            type: 'ADD_TASK',
            payload: {
                ...newTask,
                id: Date.now(),
                projectId: props.projectId,
                number: nextNumber,
                createdAt: new Date().toString(),
            }
        });

        onClear();
    }

    useEffect(() => {
        if (props.isVisible && inputRef.current) inputRef.current.focus();
    }, [props.isVisible])

    return (
        <div className={`task-add ${props.isVisible ? '' : 'hidden'}`}>
            <div className='task-add-form'>
                <h3 className='task-add-title'>New Task Details</h3>
                <div className='task-add-element'>
                    <p className='task-add-label'>Name:</p>
                    <Input
                        value={newTask.name}
                        setValue={updateNewTaskName}
                        onEnter={onAdd}
                        ref={inputRef}
                    />
                </div>
                <div className='task-add-element'>
                    <p className='task-add-label'>Description:</p>
                    <Input
                        value={newTask.description}
                        setValue={updateNewTaskDescription}
                        onEnter={onAdd}
                    />
                </div>
                <div className='task-add-element'>
                    <p className='task-add-label'>Priority:</p>
                    <Select
                        options={PRIORITY_OPTIONS}
                        value={newTask.priority.toString()}
                        setValue={updateNewTaskPriority}
                    />
                </div>
                <div className='task-add-element'>
                    <p className='task-add-label'>Parent Task:</p>
                    <Select
                        options={props.tasks.map(task => {
                            return {value: task.id.toString(), label: task.name}
                        })}
                        value={newTask.parentTaskId?.toString() || ''}
                        setValue={updateNewTaskParent}
                        nullable
                    />
                </div>
                <Button
                    className='task-add-element w-100'
                    value='Close'
                    color='red'
                    onClick={onClose}
                />
                <Button
                    className='task-add-element w-100'
                    value='Clear'
                    color='red'
                    onClick={onClear}
                />
                <Button
                    className='task-add-element w-100'
                    value='Add'
                    color='telegram'
                    onClick={onAdd}
                />
            </div>
        </div>
    )
}

export default TaskAdd;
