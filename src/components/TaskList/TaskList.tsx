import './TaskList.scss';
import {useState} from 'react';
import {useDispatch} from 'react-redux';
import {DEFAULT_TASK, Task} from '../../redux/reducers/tasksReducer';
import ModalConfirm from '../Modal/ModalConfirm';
import ModalEditTask from '../Modal/ModalEditTask';
import TaskListElement from '../TaskListElement/TaskListElement';
import Input from '../Input/Input';

interface TaskListProps {
    tasks: Task[]
    parentTaskId?: number
}

const TaskList = (props: TaskListProps) => {
    const dispatch = useDispatch()

    const [filterNumber, setFilterNumber] = useState('')
    const [filterName, setFilterName] = useState('')
    const filteredTasks = props.tasks.filter(t =>
        t.parentTaskId === props.parentTaskId &&
        (!filterNumber || t.number.toString().includes(filterNumber)) &&
        (!filterName || t.name.includes(filterName))
    )

    const [modalConfirmDeleteTaskVisible, setModalConfirmDeleteTaskVisible] = useState(false)
    const showModalConfirmDeleteTask = () => setModalConfirmDeleteTaskVisible(true)
    const hideModalConfirmDeleteTask = () => setModalConfirmDeleteTaskVisible(false)
    const [taskToDelete, setTaskToDelete] = useState(DEFAULT_TASK)
    const openModalConfirmDelete = (task: Task) => {
        setTaskToDelete(task);
        showModalConfirmDeleteTask();
    }
    const onDeleteTask = () => {
        dispatch({type: 'DELETE_TASK', payload: taskToDelete.id});
    }

    const [modalEditTaskVisible, setModalEditTaskVisible] = useState(false)
    const showModalEditTask = () => setModalEditTaskVisible(true)
    const hideModalEditTask = () => setModalEditTaskVisible(false)
    const [taskToEdit, setTaskToEdit] = useState(DEFAULT_TASK)
    const openModalEditTask = (task: Task) => {
        setTaskToEdit(task);
        showModalEditTask();
    }
    const onEditTask = (values: Map<string, string>) => {
        const newTask = {
            ...taskToEdit,
            name: values.get('name'),
            description: values.get('description'),
            priority: parseInt(values.get('priority') ?? '0'),
        }
        dispatch({type: 'UPDATE_TASK', payload: newTask});
    }

    return (
        <>
            <div className={`task-list ${props.parentTaskId ? 'subtasks' : ''}`}>
                {props.parentTaskId ? '' :
                    <>
                        <h3 className='task-list-title'>Task List {filteredTasks.length ? '' : 'Is Empty'}</h3>
                        <div className='task-list-filter'>
                            <Input value={filterNumber} setValue={setFilterNumber} placeholder='Filter by number'/>
                            <Input value={filterName} setValue={setFilterName} placeholder='Filter by name'/>
                        </div>
                    </>
                }
                {filteredTasks.map(task =>
                    <TaskListElement
                        key={task.id}
                        task={task}
                        tasks={props.tasks}
                        onDelete={openModalConfirmDelete}
                        onEdit={openModalEditTask}
                    />
                )}
            </div>
            <ModalConfirm
                onAction={onDeleteTask}
                visible={modalConfirmDeleteTaskVisible}
                hide={hideModalConfirmDeleteTask}
                title='Confirm action'
                description='Are you sure you want to delete the task?'
                actionName='Delete'
                actionNegative
            />
            <ModalEditTask
                task={taskToEdit}
                onAction={onEditTask}
                visible={modalEditTaskVisible}
                hide={hideModalEditTask}
            />
        </>
    )
}

export default TaskList;
