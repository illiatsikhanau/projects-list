import './TaskListElement.scss';
import {ChangeEvent, useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import {Priority, Status, Task} from '../../redux/reducers/tasksReducer';
import TaskList from '../TaskList/TaskList';
import TaskStatusCell from '../TaskStatusCell/TaskStatusCell';
import CommentList from '../CommentList/CommentList';

interface TaskListElementProps {
    task: Task
    tasks: Task[]
    onDelete: (task: Task) => void
    onEdit: (task: Task) => void
}

const TaskListElement = (props: TaskListElementProps) => {
    const dispatch = useDispatch()

    const [isExpand, setExpand] = useState(false)
    const switchIsExpand = () => setExpand(!isExpand)
    const [timeInWork, setTimeInWork] = useState('')

    const toLocaleDate = (date: string | undefined) => date ? new Date(date).toLocaleString() : ''
    const getTimeInWork = () => {
        const finishedAt = props.task?.finishedAt && props.task?.finishedAt !== '' ?
            props.task.finishedAt : new Date().toString();
        let dateDiff = new Date(finishedAt).getTime() - new Date(props.task?.createdAt ?? '').getTime();

        const days = Math.floor(dateDiff / 86400000);
        const hours = Math.floor(dateDiff / 3600000) % 24;
        const minutes = Math.floor(dateDiff / 60000) % 60;
        const seconds = Math.floor(dateDiff / 1000) % 60;

        return (days ? `${days} d ` : '') +
            (hours ? `${hours} h ` : '') +
            (minutes ? `${minutes} min ` : '') +
            `${seconds} sec`;
    }

    const addFile = (e: ChangeEvent<HTMLInputElement>) => {
        if (e.target.files && e.target.files[0]) {
            const file = e.target.files[0];
            const reader = new FileReader();
            reader.onload = (e: ProgressEvent<FileReader>) => {
                const newFile = {
                    name: file.name,
                    type: file.type,
                    contents: e.target?.result ?? '',
                }
                dispatch({
                    type: 'UPDATE_TASK',
                    payload: {
                        ...props.task,
                        files: props.task.files.concat(newFile),
                    }
                });
            }
            reader.readAsText(file);
        }
    }

    const downloadFile = (fileIndex: number) => {
        const taskFile = props.task.files[fileIndex];
        if (taskFile) {
            const element = document.createElement('a');
            const file = new Blob([taskFile.contents], {type: taskFile.type});
            element.href = URL.createObjectURL(file);
            element.download = taskFile.name;
            document.body.appendChild(element);
            element.click();
        }
    }

    useEffect(() => {
        if (props.task) setTimeInWork(getTimeInWork());
        // eslint-disable-next-line
    }, [props.task, isExpand])

    return (
        <div className={`task-list-element ${isExpand ? '' : 'simplify'}`}>
            <div className='task'>
                <div className='task-details'>
                    <div className='task-name'>
                        #{props.task.number}
                        <span className='accent'>{props.task.name}</span>
                    </div>
                    <div className='task-action' onClick={switchIsExpand}>
                        {isExpand ? 'Hide' : 'Show'} Details
                    </div>
                </div>
                <div className='task-status'>
                    <TaskStatusCell task={props.task} status={Status.QUEUE}/>
                    <TaskStatusCell task={props.task} status={Status.DEVELOPMENT}/>
                    <TaskStatusCell task={props.task} status={Status.DONE}/>
                </div>
            </div>
            <div className='task-expand'>
                <div className='task-expand-details'>
                    <p className='details-element'>
                        Number: <span className='accent'>{props.task.number}</span>
                    </p>
                    <p className='details-element'>
                        Name: <span className='accent'>{props.task.name}</span>
                    </p>
                    <p className='details-element'>
                        Description: <span className='accent'>{props.task.description}</span>
                    </p>
                    <p className='details-element'>
                        Priority: <span className='accent'>{Priority[props.task.priority]}</span>
                    </p>
                    <p className='details-element'>
                        Status: <span className='accent'>{Status[props.task.status]}</span>
                    </p>
                    <p className='details-element'>
                        Created at: <span className='accent'>{toLocaleDate(props.task?.createdAt)}</span>
                    </p>
                    {props.task.status === Status.DONE ? <p className='details-element'>
                        Finished at: <span className='accent'>{toLocaleDate(props.task?.finishedAt)}</span>
                    </p> : ''}
                    <p className='details-element'>
                        In work: <span className='accent'>{timeInWork}</span>
                    </p>
                    <div className='details-element col'>
                        Files:
                        <div className='files-list'>
                            {props.task.files.map((file, index) =>
                                <div key={index} className='files-list-element' onClick={() => downloadFile(index)}>
                                    {file.name}
                                </div>
                            )}
                            <label className='file-upload'>
                                Upload File
                                <input className='file-upload-input' type='file' onChange={addFile}/>
                            </label>
                        </div>
                    </div>
                    <div className='details-element col'>
                        Subtasks:
                        <TaskList tasks={props.tasks} parentTaskId={props.task.id}/>
                    </div>
                    <div className='details-element col'>
                        Comments:
                        <CommentList task={props.task} level={0}/>
                    </div>
                    <div className='details-element'>
                        <p className='details-action' onClick={() => props.onEdit(props.task)}>
                            Edit
                        </p>
                        <p className='details-action' onClick={() => props.onDelete(props.task)}>
                            Delete
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TaskListElement;
