import './TaskStatusCell.scss';
import {useDispatch} from 'react-redux';
import {useDrop} from 'react-dnd';
import {Status, Task} from '../../redux/reducers/tasksReducer';
import TaskStatusLabel from '../TaskStatusLabel/TaskStatusLabel';

interface TaskStatusCellProps {
    status: Status
    task: Task
}

const TaskStatusCell = (props: TaskStatusCellProps) => {
    const dispatch = useDispatch()

    const [{isOver}, drop] = useDrop(() => ({
        accept: props.task.id.toString(),
        drop: () => updateTaskStatus(),
        collect: monitor => ({
            isOver: monitor.isOver()
        }),
    }), [props.task])

    const updateTaskStatus = () => {
        const newTask = {
            ...props.task,
            status: props.status,
            finishedAt: props.status === Status.DONE ? new Date().toString() : '',
        }
        dispatch({type: 'UPDATE_TASK', payload: newTask});
    }

    return (
        <div ref={drop} className={`task-status-cell ${isOver ? 'hover' : ''}`} onClick={updateTaskStatus}>
            <TaskStatusLabel task={props.task} status={props.status}/>
        </div>
    )
}

export default TaskStatusCell;
