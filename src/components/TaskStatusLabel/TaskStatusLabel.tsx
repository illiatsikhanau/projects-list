import './TaskStatusLabel.scss';
import {useDrag} from 'react-dnd';
import {Status, Task} from '../../redux/reducers/tasksReducer';

interface TaskStatusCellProps {
    task: Task
    status: Status
}

const TaskStatusLabel = (props: TaskStatusCellProps) => {
    const [, drag] = useDrag(() => ({
        type: props.task.id.toString(),
        canDrag: props.task.status === props.status,
    }), [props.task])

    const active = props.task.status === props.status ? `active-${props.status}` : ''

    return (
        <p ref={drag} className={`task-status-label ${active}`}>
            {Status[props.status]}
        </p>
    )
}

export default TaskStatusLabel;
