import './HomePage.scss';
import {createRef, useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import {useAppSelector} from '../../../redux/useAppSelector';
import {DEFAULT_PROJECT} from '../../../redux/reducers/projectsReducer';
import {Link} from 'react-router-dom';
import ModalConfirm from '../../Modal/ModalConfirm';
import Button from '../../Button/Button';
import Trash from '../../../svg/Trash';
import Input from '../../Input/Input';
import Edit from '../../../svg/Edit';

const HomePage = () => {
    const dispatch = useDispatch()
    const projects = useAppSelector(state => state.projects.list)

    const inputNameRef = createRef<HTMLInputElement>()

    const [newProject, setNewProject] = useState(DEFAULT_PROJECT)
    const onUpdateNewProjectName = (name: string) => setNewProject({...newProject, name: name})

    const onAddProject = () => {
        const projectToAdd = {
            ...newProject,
            id: Date.now(),
        }
        setNewProject(DEFAULT_PROJECT);
        dispatch({type: 'ADD_PROJECT', payload: projectToAdd});
    }
    const onDeleteProject = () => {
        dispatch({type: 'DELETE_PROJECT', payload: projectIdToDelete});
    }

    const [modalConfirmDeleteProjectVisible, setModalConfirmDeleteProjectVisible] = useState(false)
    const showModalConfirmDeleteProject = () => setModalConfirmDeleteProjectVisible(true)
    const hideModalConfirmDeleteProject = () => setModalConfirmDeleteProjectVisible(false)
    const [projectIdToDelete, setProjectIdToDelete] = useState(0)
    const openModalConfirmDelete = (id: number) => {
        setProjectIdToDelete(id);
        showModalConfirmDeleteProject();
    }

    useEffect(() => {
        inputNameRef.current?.focus();
        // eslint-disable-next-line
    }, [])

    return (
        <div className='home-page'>
            <div className='home-page-content'>
                <h2 className='title'>Projects List</h2>
                <p className='label'>Add New Project</p>
                <div className='add-container'>
                    <Input
                        className='add-field'
                        value={newProject.name}
                        setValue={onUpdateNewProjectName}
                        placeholder={`Name`}
                        ref={inputNameRef}
                        onEnter={onAddProject}
                    />
                    <Button
                        className='add-button'
                        value='Add'
                        color='telegram'
                        onClick={onAddProject}
                    />
                </div>
                <div className='projects-list'>
                    {projects.map(project =>
                        <div key={project.id} className='projects-list-element'>
                            <Link to={`project/${project.id}`} className='link'>
                                {project.name}
                            </Link>
                            <Link to={`project/${project.id}`} className='icon'>
                                <Edit className='edit-icon'/>
                            </Link>
                            <div className='icon' onClick={() => openModalConfirmDelete(project.id)}>
                                <Trash className='trash-icon'/>
                            </div>
                        </div>
                    )}
                </div>
            </div>
            <ModalConfirm
                onAction={onDeleteProject}
                visible={modalConfirmDeleteProjectVisible}
                hide={hideModalConfirmDeleteProject}
                title='Confirm action'
                description='Are you sure you want to delete the project?'
                actionName='Delete'
                actionNegative
            />
        </div>
    )
}

export default HomePage;
