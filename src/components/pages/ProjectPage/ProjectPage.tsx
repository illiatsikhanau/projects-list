import './ProjectPage.scss';
import {useEffect, useState} from 'react';
import {shallowEqual} from 'react-redux';
import {Link, useNavigate, useParams} from 'react-router-dom';
import {DndProvider} from 'react-dnd';
import {MultiBackend} from 'react-dnd-multi-backend';
import {HTML5toTouch} from 'rdndmb-html5-to-touch';
import {useAppSelector} from '../../../redux/useAppSelector';
import TaskList from '../../TaskList/TaskList';
import TaskAdd from '../../TaskAdd/TaskAdd';
import ProjectEdit from '../../ProjectEdit/ProjectEdit';

const ProjectPage = () => {
    const {id} = useParams()
    const navigate = useNavigate()

    const tasks = useAppSelector(state => state.tasks.list.filter(t =>
        id && t.projectId === parseInt(id)
    ), shallowEqual)
    const project = useAppSelector(state => state.projects.list.find(p =>
        id && p.id === parseInt(id)
    ), shallowEqual)

    const [isEditSectionVisible, setIsEditSectionVisible] = useState(false)
    const showEditSection = () => setIsEditSectionVisible(true)
    const hideEditSection = () => setIsEditSectionVisible(false)

    const [isListSectionVisible, setIsListSectionVisible] = useState(!tasks.length)
    const showListSection = () => setIsListSectionVisible(true)
    const hideListSection = () => setIsListSectionVisible(false)

    useEffect(() => {
        if (!project) navigate('/');
        // eslint-disable-next-line
    }, [])

    return (
        <div className='project-page'>
            <div className='project-page-content'>
                <div className='project-title'>
                    <p className='project-name'>{project?.name}</p>
                    <div className='project-actions'>
                        <p className='project-action' onClick={showEditSection}>Edit</p>
                        <p className='project-action' onClick={showListSection}>Add Task</p>
                    </div>
                    <Link to='/' className='back-link'>back to projects</Link>
                </div>
                <ProjectEdit
                    project={project}
                    isVisible={isEditSectionVisible}
                    hide={hideEditSection}
                />
                <TaskAdd
                    projectId={parseInt(id ?? '0')}
                    tasks={tasks}
                    isVisible={isListSectionVisible}
                    hide={hideListSection}
                />
                <DndProvider backend={MultiBackend} options={HTML5toTouch}>
                    <TaskList tasks={tasks}/>
                </DndProvider>
            </div>
        </div>
    )
}

export default ProjectPage;
