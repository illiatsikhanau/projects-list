import React from 'react';
import ReactDOM from 'react-dom/client';
import {Provider} from 'react-redux';
import store from './redux/store';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import HomePage from './components/pages/HomePage/HomePage';
import ProjectPage from './components/pages/ProjectPage/ProjectPage';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <Provider store={store}>
        <BrowserRouter>
            <Routes>
                <Route index element={<HomePage/>}/>
                <Route path='/project/:id' element={<ProjectPage/>}/>
            </Routes>
        </BrowserRouter>
    </Provider>
)
