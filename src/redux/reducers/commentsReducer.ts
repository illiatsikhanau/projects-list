export interface Comment {
    id: number
    taskId: number
    parentCommentId?: number
    text: string
}

export const DEFAULT_COMMENT: Comment = {
    id: 0,
    taskId: 0,
    text: '',
}

export interface CommentsState {
    list: Comment[]
}

interface AddCommentAction {
    type: 'ADD_COMMENT'
    payload: Comment
}

const initialState: CommentsState = {
    list: JSON.parse(localStorage.getItem('comments') ?? '[]')
}

const saveList = (list: Comment[]) => {
    localStorage.setItem('comments', JSON.stringify(list));
    return list;
}

const listReducer = (state = initialState, action: AddCommentAction) => {
    switch (action.type) {
        case 'ADD_COMMENT':
            return {
                ...state,
                list: saveList([...state.list, action.payload]),
            }
        default:
            return state
    }
}

export default listReducer;
