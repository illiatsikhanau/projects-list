import {combineReducers} from 'redux';
import projectsReducer from './projectsReducer';
import tasksReducer from './tasksReducer';
import commentsReducer from './commentsReducer';

const rootReducer = combineReducers({
    projects: projectsReducer,
    tasks: tasksReducer,
    comments: commentsReducer,
})

export default rootReducer;
