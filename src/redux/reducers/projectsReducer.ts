export interface Project {
    id: number
    name: string
}

export const DEFAULT_PROJECT: Project = {
    id: 0,
    name: '',
}

export interface ProjectsState {
    list: Project[]
}

interface AddProjectAction {
    type: 'ADD_PROJECT'
    payload: Project
}

interface DeleteProjectAction {
    type: 'DELETE_PROJECT'
    payload: number
}

interface UpdateProjectAction {
    type: 'UPDATE_PROJECT'
    payload: Project
}

const initialState: ProjectsState = {
    list: JSON.parse(localStorage.getItem('projects') ?? '[]')
}

const saveList = (list: Project[]) => {
    localStorage.setItem('projects', JSON.stringify(list));
    return list;
}

const listReducer = (
    state = initialState,
    action: AddProjectAction | DeleteProjectAction | UpdateProjectAction
) => {
    switch (action.type) {
        case 'ADD_PROJECT':
            return {
                ...state,
                list: saveList([...state.list, action.payload]),
            }
        case 'DELETE_PROJECT':
            return {
                ...state,
                list: saveList(state.list.filter(element => element.id !== action.payload)),
            }
        case 'UPDATE_PROJECT':
            return {
                ...state,
                list: saveList(state.list.map(element => element.id === action.payload.id ? action.payload : element)),
            }
        default:
            return state
    }
}

export default listReducer;
