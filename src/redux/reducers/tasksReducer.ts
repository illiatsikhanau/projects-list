export enum Priority {
    LOW,
    MEDIUM,
    HIGH,
}

export const PRIORITY_OPTIONS = [
    {value: Priority.LOW.toString(), label: Priority[Priority.LOW]},
    {value: Priority.MEDIUM.toString(), label: Priority[Priority.MEDIUM]},
    {value: Priority.HIGH.toString(), label: Priority[Priority.HIGH]},
]

export enum Status {
    QUEUE,
    DEVELOPMENT,
    DONE,
}

export interface File {
    name: string
    type: string
    contents: string | ArrayBuffer
}

export interface Task {
    id: number
    projectId: number
    parentTaskId?: number
    number: number
    name: string
    description: string
    createdAt: string
    finishedAt: string
    priority: Priority
    status: Status
    files: File[]
}

export const DEFAULT_TASK: Task = {
    id: 0,
    projectId: 0,
    number: 0,
    name: '',
    description: '',
    createdAt: '',
    finishedAt: '',
    priority: Priority.LOW,
    status: Status.QUEUE,
    files: [],
}

export interface TasksState {
    list: Task[]
}

interface AddTaskAction {
    type: 'ADD_TASK'
    payload: Task
}

interface DeleteTaskAction {
    type: 'DELETE_TASK'
    payload: number
}

interface UpdateTaskAction {
    type: 'UPDATE_TASK'
    payload: Task
}

const initialState: TasksState = {
    list: JSON.parse(localStorage.getItem('tasks') ?? '[]')
}

const getTreeIds = (taskId: number, tasks: Task[]) => {
    const treeIds = [taskId];
    for (let i = 0; i < tasks.length; i++) {
        if (treeIds.find(id => id === tasks[i].parentTaskId)) {
            treeIds.push(tasks[i].id);
        }
    }
    return treeIds;
}

const saveList = (list: Task[]) => {
    localStorage.setItem('tasks', JSON.stringify(list));
    return list;
}

const listReducer = (
    state = initialState,
    action: AddTaskAction | DeleteTaskAction | UpdateTaskAction
) => {
    switch (action.type) {
        case 'ADD_TASK':
            return {
                ...state,
                list: saveList([...state.list, action.payload]),
            }
        case 'DELETE_TASK':
            const toDeleteIds = getTreeIds(action.payload, state.list);
            return {
                ...state,
                list: saveList(state.list.filter(element => !toDeleteIds.find(id => id === element.id))),
            }
        case 'UPDATE_TASK':
            const updatedList = state.list.map(element => element.id === action.payload.id ? action.payload : element);
            const toEditIds = getTreeIds(action.payload.id, state.list);

            return {
                ...state,
                list: saveList(updatedList.map(element => {
                    return {
                        ...element,
                        status: toEditIds.find(id => id === element.id) ? action.payload.status : element.status,
                    }
                })),
            }
        default:
            return state
    }
}

export default listReducer;
